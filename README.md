# Ishpeck's Alternity Thing

A  stupid tool  I  vomited  out in  Python  to  help manage  Alternity
character data.

# Quick Start 

To make a new character, run the manager.py script with no arguments:

`$ python manager.py`

When you exit, character's current data will automatically be saved to
a file with the .alternity extension.

To  retrieve character  data from  disk, make  sure the  last argument
passed into the manager.py script is the name of the character file.

`$ python manager.py my_character_data.alternity`

To e-mail the character data to your GM, try to "dump" data like so:

`$ python manager.py -d my_character_data.alternity | mail -s "My character" gamemaster@webealternityfans.org`

Special manager commands are preceded  by forward slashes.  To see the
list  of  available  commands,  use the  "/help"  command  inside  the
manager.

`$ echo '/help' | python manager.py my_character_data.alternity`

# Managing Character Data 

When in  the character  manager, you  can get and  set values  for the
different stats by  typing the name of the stat  followed by the value
you you want the stat to have.

The following will set the Strength score to 10.

`> str 10`  
`Changed str from 7 to 10`  

You  can  also change  the  stat  relative  to  its current  value  by
preceding numbers with either + or -

`> str 10`  
`Changed str from 7 to 10`  
`> str +2`  
`Changed str from 10 to 12`  

Will set the Strength score to 12.

To purchase Broad Skills, set its value to any non-zero number.

`> athletics 1098247598745`  
`Set athletics to be 1098247598745`  
`> athletics 1`  
`Set athletics to be 1`  

The two above lines both set  the athletics skill to "trained" and the
following will set the athletics skill to "untrained"

`> athletics 0`  
`Set athletics to be 0`  

Stat names are case insensitive.  The following is valid.

`> iNtErAcTiOn 1`  
`Set interaction to be 1`  

If the stat  name has a space in it,  the command interpreter _SHOULD_
understand what you mean.

`> computer science 10`  
`Set computer_science to be 10`  

But if it doesn't  put an underscore in the name  and that should work
out nicely.

`> computer_science 1`  
`Set computer_science to be 1`  

Any stat with a  space in its name can be  referenced by replacing the
space with an underscore. 

If you don't put a value after the stat name, the stat's current value
is reported.

`> knowledge`  
`knowledge is currently trained`  
`> charm`  
`charm is currently 0`  

Specialty skills will have a rank equal to the value you give it.

`> charm 2`  
`Set charm to be 2`  

The above  will set  the Charm specialty  skill under  the Interaction
broad skill to have 2 ranks.

# Adding Skills to the List 

If you want to add a skill to  the list, we have a CSV-like data sheet
inculded.  Skill  groups are sparated by  double-new-lines.  The first
skill in the group is read as the Broad skill.  Every other is read as
the Specialty skill.  Once you've edited the CSV to your satisfaction,
you need to generate the new skills tree data by doing this:

`$ python convertSkills.py < skills.csv > skills.py`  

# Perks and Flaws 

You can add perks by doing this:

`> perk perkname +`  

If  the skill  has multpile  levels, add  the perk  multiple times  to
increase its level.

If a flaw and perk have the same name, the first time you take it will
give you the flaw  version and the second time will  give you the perk
version.

# Exiting the Manager 

You quit by either typing the  "/quit" command or sending EOF.  That's
CTRL+Z in Windows systems and CTRL+D on computers for grown-ups.

# Legal Mumbo Jumbo 

The Alternity  RPG belongs to  whoever it is  that ended up  buying it
(not me).   I'm pretty  sure any  owner of the  game is  reserving all
their rights like folks tend to do.
